Overview
========

This package implements dynamic schema adaptation [1] for use with the
stereotypes content framework.

Reads and writes are relatively inexpensive. A casual test yields
approx. 30.000 read or write operations per second, while adaptations
are about twice the cost.


Technical details
-----------------

[1] Dynamic adaptation is a pattern used to adapt content objects to
any schema interface (interfaces declaring attributes only).
