from zope import interface
from zope import component

from interfaces import IFieldStorage

class FieldStorageProxy(object):
    """Proxy between a context and a field storage."""
    
    __slots__ = 'context', '__provides__'
    
    def __init__(self, context):
        self.context = context

    def __getattr__(self, name):
        if name in self.__slots__:
            return object.__getattr__(self, name)

        storage = IFieldStorage(self.context)
        field = interface.providedBy(self).get(name)
        return storage.get(field)

    def __setattr__(self, name, value):
        if name in self.__slots__:
            return object.__setattr__(self, name, value)

        storage = IFieldStorage(self.context)
        field = interface.providedBy(self).get(name)
        storage[field] = value
