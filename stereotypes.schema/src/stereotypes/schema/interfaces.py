from zope import interface

class IFieldStorage(interface.Interface):
    """Stores arbitrary data under field-unique keys."""

    def __getitem__(field):
        """Return the data stored for this field.

        Raises KeyError if no data is available for this field.
        """

    def __setitem__(field, value):
        """Store data for this field.

        In order to avoid key collisions, users of this interface must
        use their dotted package name as part of the key name.
        """

    def __delitem__(field):
        """Removes the data for this field.

        Raises a KeyError if the key is not found.
        """

    def get(field):
        """Return the data for this field.

        The default field value is returned if no data is available.
        """
