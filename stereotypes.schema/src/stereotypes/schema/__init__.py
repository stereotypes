from zope.interface import alsoProvides
from zope.interface.interface import adapter_hooks
from zope.interface.interfaces import IAttribute
from zope.annotation.interfaces import IAnnotatable

import adapter
import interfaces

def hook(interface, obj):
    """Dynamic adaptation hook.

    Adapts the provided object to the interface if the following
    conditions are met:

    1. The interface declares only schema attributes.
    2. The object adapts to IFieldStorage.

    """

    # first attempt remaining hooks
    for _hook in adapter_hooks:
        if _hook is not hook:
            context = _hook(interface, obj)
            if context is not None:
                return context

    # verify field storage is available
    if not interfaces.IFieldStorage(obj, None):
        return

    # verify interface declares only attributes
    for name in interface:
        if not IAttribute.providedBy(interface[name]):
            return

    context = adapter.FieldStorageProxy(obj)
    alsoProvides(context, interface)

    return context

adapter_hooks.append(hook)
