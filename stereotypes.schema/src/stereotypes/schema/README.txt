stereotypes.schema
==================

Dynamic schema adaptation requires a field storage adapter. The
implementation for an annotations-based storage is straight forward.

  >>> from zope.annotation.interfaces import IAnnotations
  >>> from stereotypes.schema.interfaces import IFieldStorage

  >>> class AnnotationFieldStorage(object):
  ...     interface.implements(IFieldStorage)
  ...     key = 'stereotypes.schema'
  ...
  ...     def __init__(self, context):
  ...         self.context = context
  ...
  ...     def __getitem__(self, field):
  ...         annotations = IAnnotations(self.context)
  ...         return annotations[self.key][field]
  ...
  ...     def __setitem__(self, field, value):
  ...         annotations = IAnnotations(self.context)
  ...         annotations.setdefault(self.key, {})[field] = value
  ...
  ...     def __delitem__(self, field):
  ...         del IAnnotations(self.context)[self.key][field]
  ...
  ...     def get(self, field):
  ...         try:
  ...             return self[field]
  ...         except KeyError:
  ...             return field.default

We'll register the field storage as a component, adapting a content
object.

  >>> from zope.annotation.interfaces import IAttributeAnnotatable
  >>> class Content(object):
  ...     interface.implements(IAttributeAnnotatable)

  >>> component.provideAdapter(AnnotationFieldStorage, (Content,))

For the demonstration, we'll need a content object.

  >>> obj = Content()

Schema attributes
-----------------
  
Let's set up a schema with attributes defining an event object.

  >>> class IEvent(interface.Interface):
  ...     location = schema.TextLine(title=u"Location")
  ...     date = schema.Date(title=u"Date")

Because our schema defines only attributes, not methods, we can adapt
any annotatable object to it using dynamic adaptation.

  >>> event = IEvent(obj)

Let's examine the properties of the returned event object. It
implements the interface we adapted to.

  >>> IEvent.providedBy(event)
  True

Attributes can be set and read.

  >>> from datetime import date
  
  >>> event.location = u"Rio de Janeiro"
  >>> event.date = date(2008, 4, 17)

  >>> "%s, %s" % (event.location, event.date)
  u'Rio de Janeiro, 2008-04-17'

If we subclass the schema and adapt to the new interface, attribute
integrity is preserved.

  >>> class IExtendedEvent(IEvent):
  ...     pass

  >>> extended = IExtendedEvent(obj)
  >>> "%s, %s" % (extended.location, extended.date)
  u'Rio de Janeiro, 2008-04-17'

  >>> extended.location = u"Sao Paulo"
  >>> "%s, %s" % (event.location, event.date)
  u'Sao Paulo, 2008-04-17'

Storage details
---------------

The storage implementation gives a bit more control.

  >>> storage = IFieldStorage(obj)

We can delete data.

  >>> del storage[IEvent['location']]
  >>> storage['location']
  Traceback (most recent call last):
    ...
  KeyError: 'location'

However, at the higher level, we'll get the field default value.

  >>> extended.location is None
  True

The storage provides a ``get``-method that behaves in this way, too.

  >>> storage.get(IEvent['location']) is None
  True
  
Containment
-----------

TODO: Implement support for adaptation to interfaces that promise
container functionality.
