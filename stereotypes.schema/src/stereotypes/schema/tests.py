import doctest
import unittest
from zope import interface, component, schema
from zope.testing import doctestunit
from zope.testing import cleanup

from zope.annotation.attribute import AttributeAnnotations

def setUp(test=None):
    cleanup.setUp()
    component.provideAdapter(AttributeAnnotations)

def tearDown(test=None):
    cleanup.tearDown()

def test_suite():
    globs = dict(
        interface=interface,
        component=component,
        schema=schema)
    
    return unittest.TestSuite((
        doctestunit.DocFileSuite(
            'README.txt',
            globs=globs,
            setUp=setUp, tearDown=tearDown,
            optionflags=doctest.NORMALIZE_WHITESPACE|doctest.ELLIPSIS),
        ))

if __name__ == '__main__':
    unittest.main(defaultTest='test_suite')
